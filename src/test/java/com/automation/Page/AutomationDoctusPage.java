package com.automation.Page;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;

import com.automation.Base.Base;

public class AutomationDoctusPage extends Base{

	//----------LocatorRegister--------------------
	By registerLocator=By.linkText("Registrarte");
	By nombreLocator=By.xpath("//input[@placeholder='* Nombre']");
	By apellidoLocator=By.xpath("//input[@placeholder='* Apellido']");
	By cedulaLocator=By.xpath("//input[@data-bind='value: UserIdentification']");
	By correoLocator=By.id("email");
	By confirmarEmailLocator=By.id("confirmEmail");
	By contrasenaLocator=By.xpath("//input[@placeholder='* Contraseña']");
	By confirmarcontrasenaLocator=By.xpath("//input[@placeholder='* Vuelve a escribir la contraseña']");
	By terminosLocator=By.xpath("//input[@type='checkbox']");
	By enviarLocator=By.name("name");
	
	//-----------LocatorLogin-----------------------
	By correologinLocator=By.id("Email");
	By contraseñaloginLocator=By.id("Password");
	By iniciosesionLocator=By.xpath("//input[@value='Iniciar sesión']");
	public AutomationDoctusPage(WebDriver driver) {
		super(driver);
		// TODO Auto-generated constructor stub
	}
	
	public void RegisterInPage() {
		desplazamiento(400);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(registerLocator));
		click(registerLocator);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(nombreLocator));
		type("Sebastian",nombreLocator);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(apellidoLocator));
		type("Ossa",apellidoLocator);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(cedulaLocator));
		type("1035544689",cedulaLocator);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(correoLocator));
		type("sebastianossa159@gmail.com",correoLocator);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(confirmarEmailLocator));
		type("sebastianossa159@gmail.com",confirmarEmailLocator);
		desplazamiento(400);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(contrasenaLocator));
		type("animal123456",contrasenaLocator);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(confirmarcontrasenaLocator));
		type("animal123456",confirmarcontrasenaLocator);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(terminosLocator));
		clickEjecutor(terminosLocator);
	}
	
	public void clickRegister() {
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(enviarLocator));
		click(enviarLocator);
	}
	
	public void LoginInPage() {
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(correologinLocator));
		type("sebastianossa159@gmail.com",correologinLocator);
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(contraseñaloginLocator));
		type("animal123456",contraseñaloginLocator);
		desplazamiento(100);
	}
	
	public void clickLogin() {
		tiempoEspera(10).until(ExpectedConditions.visibilityOfElementLocated(contraseñaloginLocator));
		clickEjecutor(iniciosesionLocator);
	}

}
