package com.automation.Runners;

import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)
@CucumberOptions(features="src/test/resources/features/AutomationDoctus.feature", glue="com.automation.StepDefinition")
public class AutomationDoctusTest {

	

}
