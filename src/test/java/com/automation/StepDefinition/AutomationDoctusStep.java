package com.automation.StepDefinition;

import org.openqa.selenium.WebDriver;

import com.automation.Page.AutomationDoctusPage;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class AutomationDoctusStep {
	
	private WebDriver driver;
	private AutomationDoctusPage doctusPage;

	@Given("^open Euno page$")
	public void open_Euno_page() throws Exception {
		doctusPage=new AutomationDoctusPage(driver);
		driver=doctusPage.chormeDriverConnection();
		driver.manage().window().maximize();
		doctusPage.visit("https://euno.lms.doctustest.com/");
	}


	@When("^register in page$")
	public void register_in_page() throws Exception {
	    doctusPage.RegisterInPage();
	}

	@Then("^send the data$")
	public void send_the_data() throws Exception {
	   doctusPage.clickRegister();
	}

	@When("^login in page Euno$")
	public void login_in_page_Euno() throws Exception {
	   doctusPage.LoginInPage();
	}

	@Then("^confirm the information$")
	public void confirm_the_information() throws Exception {
	   doctusPage.clickLogin();
	}
}
