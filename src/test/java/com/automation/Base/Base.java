package com.automation.Base;

import static org.junit.Assert.assertTrue;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Base {

	private WebDriver driver;
	 
	 
	 public Base(WebDriver driver) {
		 this.driver= driver;
	 }
	 
	public WebDriver chormeDriverConnection() {
		 System.setProperty("webdriver.chrome.driver", "./src/test/resources/chromedriver/chromedriver.exe");
		 driver=new ChromeDriver();
		 return driver;
	 }	
	 
	 public WebElement findElement(By locator) {
		return driver.findElement(locator); 
	 }
	 
public String getText(WebElement element) {
	return element.getText();
}

public String getTect(By locator) {
  return driver.findElement(locator).getText();
}

public void type(String inputText, By locator){
	driver.findElement(locator).sendKeys(inputText);
}

public void click(By locator) {
	driver.findElement(locator).click();
}

public boolean isDisplayed(By locator) {
	try {
		return driver.findElement(locator).isDisplayed();
	}catch(org.openqa.selenium.NoSuchElementException e) {
		return false;
	}
}

public void visit(String URL) {
	driver.get(URL);
}

public void back() {
	driver.navigate().back();
}

public void desplazamiento(int movimiento) {
	JavascriptExecutor Scrool = (JavascriptExecutor) driver;
	Scrool.executeScript("window.scrollBy(0,"+movimiento+")", "");
	driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
}

public WebElement WebEleme(By locator) {
		WebElement search= driver.findElement(locator);
		return search;
}

public String textoPagina(By locator) {
	String texto=driver.findElement(locator).getText();
	return texto;
}

public WebDriverWait tiempoEspera(int segundos) {
	 WebDriverWait ewalt = new WebDriverWait(driver,segundos);
	 return ewalt;
}

public void presenciaElemento(By locator) {
	assertTrue(driver.findElement(locator).isDisplayed());
	
}

public String selecDropdownList(By locator, String text) {
	   Select selectList = new Select(findElement(locator));
	   selectList.selectByVisibleText(text);
	   return getText(selectList.getFirstSelectedOption());
}

public Actions actions(By locator) {
	   	Actions act=new Actions(driver);
	   	return act.moveToElement(driver.findElement(locator));
}
public void clickEjecutor(By locator) {
	   WebElement ele = driver.findElement(locator);
	   JavascriptExecutor executor = (JavascriptExecutor)driver;
	   executor.executeScript("arguments[0].click();", ele);
}
}
